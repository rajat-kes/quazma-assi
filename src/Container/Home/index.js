import React, { Component } from 'react'

import { v4 as uuidv4 } from 'uuid';
import './style.css';

import CartList from '../../Component/CartList'
import ItemsList from '../../Component/ItemsList'
export class Home extends Component {
    state = {
        itemsList: [],
        cartList: [],
    }

    handleCreateNewItems = (data) => {
        const { name } = data;
        const newData = {
            id: uuidv4(),
            name
        }
        this.setState({
            itemsList: [...this.state.itemsList, newData],
        });
    }

    handleDeleteItems = (data) => {
        const { id } = data;
        const newData = this.state.itemsList.filter(d => d.id !== id);
        this.setState({
            itemsList: newData,
        });
    }

    handleAddToCart = (data) => {
        const { id } = data
        let selectedData = {}
        const newItemsData = this.state.itemsList.filter(d => {
            if (d.id === id) {
                selectedData = d;
            } else {
                return d
            }
        });
        this.setState({
            itemsList: newItemsData,
            cartList: [...this.state.cartList, selectedData]
        });
    }

    handleRemovedToCart = (data) => {
        const { id } = data
        let selectedData = {}
        const newCartData = this.state.cartList.filter(d => {
            if (d.id === id) {
                selectedData = d;
            } else {
                return d
            }
        });
        this.setState({
            cartList: newCartData,
            itemsList: [...this.state.itemsList, selectedData]
        });
    }

    render() {
        const { itemsList, cartList } = this.state;
        return (
            <div className='header'>
                <h1>{'POS App'}</h1>
                <div className='card'>
                    <ItemsList
                        list={itemsList}
                        handleCreateNewItems={this.handleCreateNewItems}
                        handleAddToCart={this.handleAddToCart}
                        handleDeleteItems={this.handleDeleteItems}
                    />
                    <CartList
                        list={cartList}
                        handleRemovedToCart={this.handleRemovedToCart}
                    />
                </div>
            </div>
        )
    }
}

export default Home
