import React from 'react'

import MyModel from '../../Component/MyModal';

const ItemsList = (props) => {
    const [open, setOpen] = React.useState(false);
    const { list, handleCreateNewItems, handleAddToCart, handleDeleteItems } = props;

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleClickSave = (name) => {
        handleCreateNewItems({ name });
        handleClose()
    };
    return (
        <div>
            <div className='items'>
                <h1>Items</h1>
                <table className='table'>
                    <tbody>
                        {
                            list.map(d => {
                                return (
                                    <tr key={Math.random()}>
                                        <td>
                                            <p>{d.name}</p>
                                        </td>
                                        <td>
                                            <button
                                                className='btn btn--white'
                                                onClick={() => handleAddToCart({ ...d })}
                                            >
                                                {'Add to cart'}
                                            </button>
                                        </td>
                                        <td>
                                            <button
                                                className='btn btn--white'
                                                onClick={() => handleDeleteItems({ ...d })}
                                            >
                                                {'Delete Item'}
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            <button className='btn btn--white' onClick={handleClickOpen}>Add Items</button>
            <MyModel
                open={open}
                handleClose={handleClose}
                handleClickSave={handleClickSave}
            />
        </div>
    )
}

export default ItemsList
