import React from 'react'

const CartList = (props) => {
    const { list, handleRemovedToCart } = props;
    return (
        <div>
            <div className='items'>
                <h1>Cart</h1>
                <table className='table'>
                    <tbody>
                        {
                            list.map(d => {
                                return (
                                    <tr key={Math.random()}>
                                        <td>
                                            <p>{d.name}</p>
                                        </td>
                                        <td>
                                            <button
                                                className='btn btn--white'
                                                onClick={() => handleRemovedToCart({ ...d })}
                                            >
                                                {'Remove from cart'}
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default CartList
