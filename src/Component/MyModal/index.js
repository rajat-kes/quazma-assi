import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const FormDialog = (props) => {
	const [name, setName] = React.useState('');
	const { open, handleClose, handleClickSave } = props;
	return (
		<div>
			<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
				<DialogContent>
					<DialogContentText>
						{"Please enter your items name here. We will Add."}
					</DialogContentText>
					<TextField
						autoFocus
						margin="dense"
						id="name"
						label="name"
						type="name"
						value={name}
						onChange={(event) => setName(event.target.value)}
						fullWidth
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => { setName(''); handleClose() }} color="primary">
						Cancel
          </Button>
					<Button onClick={() => { setName(''); handleClickSave(name) }} color="primary">
						Save
          </Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}

export default FormDialog;